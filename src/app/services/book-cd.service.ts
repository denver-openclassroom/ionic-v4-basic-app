import { Injectable } from '@angular/core';
import {Book} from '../models/Book';
import {Cd} from '../models/Cd';

@Injectable({
  providedIn: 'root'
})
export class BookCdService {
    bookList: Book[] = [
        {
          title: 'Le Java pour tous',
            author: 'Jean pierre',
            description: [
                'Nombre de page: 588',
                'Edition: Cle',
                'ISNB: 524515151GHBHB'
            ],
            lend:false
        },

        {
            title: 'Le C# pour debutant',
            author: 'Le courge',
            description: [
                'Nombre de page: 288',
                'Edition: Cle',
                'ISNB: 852145451TGDJKK'
            ],
            lend:false
        },

        {
            title: 'Bien debuter le C++',
            author: 'Denver Dio',
            description: [
                'Nombre de page: 300',
                'Edition: Matinale',
                'ISNB: 51515415FTVHD'
            ],
            lend:true
        },
    ] ;


    cdList : Cd[] = [
        {
          title: 'Le monde de Narnia',
            actor: 'Jeff Malon',
            content: [
                'Jean Claude Van: Universal soldiers',
                'Billy Back: TC200',
                'Jp C, GT B : Le monde incroyable de narnia'
            ],
            lend: false
        },

        {
            title: 'La musique des annees 2000',
            actor: 'Denver Dio',
            content: [
                'M Gims: Transcendance',
                'Dadju: Gentlemen 3.0',
            ],
            lend: false
        },

        {
            title: 'Apprendre le francais en 3h',
            actor: 'Divany Dio',
            content: [
                'Alphabet : de a a z',
                'lire : Apprendre la lire la langue',
                'ecrire: Apprendre a ecrire la langue',
                'parler: Apprendre a paler la langue'
            ],
            lend: true
        },
    ];

  constructor() { }
}

import { TestBed } from '@angular/core/testing';

import { BookCdService } from './book-cd.service';

describe('BookCdService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BookCdService = TestBed.get(BookCdService);
    expect(service).toBeTruthy();
  });
});

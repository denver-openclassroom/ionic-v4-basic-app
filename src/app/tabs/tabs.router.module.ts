import {RouterModule, Routes} from '@angular/router';
import {TabsPage} from './tabs.page';
import {NgModule} from '@angular/core';


const routes: Routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            {
                path: 'book-list',
                children: [
                    {
                        path: '',
                        loadChildren: '../book-list/book-list.module#BookListPageModule'
                    }
                ]
            },
            {
                path: 'lend-book',
                loadChildren: '../lend-book/lend-book.module#LendBookPageModule'
            },


            {
                path: 'cd-list',
                children: [
                    {
                        path: '',
                        loadChildren: '../cd-list/cd-list.module#CdListPageModule'
                    }
                ]
            },
            {
                path: 'lend-cd',
                loadChildren: '../lend-cd/lend-cd.module#LendCdPageModule'
            },

            {
                path: '',
                redirectTo: '/tabs/book-list',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/book-list',
        pathMatch: 'full'
    }

];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class TabsPageRoutingModule {
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {TabsPage} from './tabs.page';
import {SharedModule} from '../shared/shared.module';
import {TabsPageRoutingModule} from './tabs.router.module';
import {DirectivesModule} from '../directives/directives.module';

const routes: Routes = [
    {
        path: '',
        component: TabsPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
        TabsPageRoutingModule,
        DirectivesModule

    ],
    declarations: [TabsPage]
})
export class TabsPageModule {
}

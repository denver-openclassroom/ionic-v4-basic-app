import { Component, OnInit } from '@angular/core';
import {Book} from '../models/Book';
import {ActivatedRoute} from '@angular/router';
import {ModalController} from '@ionic/angular';
import {BookCdService} from '../services/book-cd.service';

@Component({
  selector: 'app-lend-book',
  templateUrl: './lend-book.page.html',
  styleUrls: ['./lend-book.page.scss'],
})
export class LendBookPage implements OnInit {

    private book: Book;
    private index:number;

    constructor(private route: ActivatedRoute,
                private modalCtrl: ModalController,
                private bookCdService: BookCdService) { }

  ngOnInit() {
      this.book = this.bookCdService.bookList[this.index];
  }

  dismissModal(){
      this.modalCtrl.dismiss();
  }

  onLendBook(){
      this.book.lend = !this.book.lend;
  }

}

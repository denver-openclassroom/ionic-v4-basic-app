/**
 * @author Dany.difo
 */

export class Book{
    constructor(public title:string,public author: string, public description:string[], public lend:boolean){
        this.lend = false;
    }
}
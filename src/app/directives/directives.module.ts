import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SwipeTabDirective} from './swipe-tab.directive';
import {SharedModule} from '../shared/shared.module';

@NgModule({
    declarations: [
        SwipeTabDirective
    ],
    imports: [
        CommonModule,
        SharedModule
    ],
    exports: [
        SwipeTabDirective
    ]
})
export class DirectivesModule {
}

import {Component, OnInit} from '@angular/core';
import {Book} from '../models/Book';
import {BookCdService} from '../services/book-cd.service';
import {MenuController, ModalController, NavController} from '@ionic/angular';
import {LendBookPage} from '../lend-book/lend-book.page';

@Component({
    selector: 'app-book-list',
    templateUrl: './book-list.page.html',
    styleUrls: ['./book-list.page.scss'],
})
export class BookListPage implements OnInit {

    bookList: Book[] = [];

    constructor(private navCtrl: NavController,
                private bookCdService: BookCdService,
                private modalCtrl: ModalController,
                private menuCtrl: MenuController) {
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.bookList = this.bookCdService.bookList.slice();
    }

    async onLoadBook(index: number) {
        const modal = await this.modalCtrl.create({
            component: LendBookPage,
            componentProps: {
                index: index
            }
        });

        return await modal.present();
    }

    onToggleMenu(){
        this.menuCtrl.enable(true,'content');
        this.menuCtrl.open('content');
    }


}

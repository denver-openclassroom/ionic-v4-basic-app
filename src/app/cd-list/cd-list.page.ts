import { Component, OnInit } from '@angular/core';
import {LendBookPage} from '../lend-book/lend-book.page';
import {Cd} from '../models/Cd';
import {MenuController, ModalController, NavController} from '@ionic/angular';
import {BookCdService} from '../services/book-cd.service';
import {LendCdPage} from '../lend-cd/lend-cd.page';

@Component({
  selector: 'app-cd-list',
  templateUrl: './cd-list.page.html',
  styleUrls: ['./cd-list.page.scss'],
})
export class CdListPage implements OnInit {

  cdList: Cd[] = [];

  constructor(private navCtrl: NavController,
              private modalCtrl : ModalController,
              private bookCdService : BookCdService,
              private menuCtrl: MenuController
  ) { }

  ngOnInit() {
  }


    ionViewWillEnter() {
        this.cdList = this.bookCdService.cdList.slice();
    }

    async onLoadCd(index: number) {
        const modal = await this.modalCtrl.create({
            component: LendCdPage,
            componentProps: {
                index: index
            }
        });

        return await modal.present();
    }

    onToggleMenu(){
        this.menuCtrl.enable(true,'content');
        this.menuCtrl.open('content');
    }


}

import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ModalController} from '@ionic/angular';
import {BookCdService} from '../services/book-cd.service';
import {Cd} from '../models/Cd';

@Component({
  selector: 'app-lend-cd',
  templateUrl: './lend-cd.page.html',
  styleUrls: ['./lend-cd.page.scss'],
})
export class LendCdPage implements OnInit {

  private cd : Cd;
  private index: number;

  constructor(private route: ActivatedRoute,
              private modalCtrl: ModalController,
              private bookCdService: BookCdService) { }

  ngOnInit() {
      this.cd = this.bookCdService.cdList[this.index];

  }

    dismissModal(){
        this.modalCtrl.dismiss();
    }

    onLendCd(){
        this.cd.lend = !this.cd.lend;
    }

}
